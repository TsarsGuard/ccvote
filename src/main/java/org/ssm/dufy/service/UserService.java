package org.ssm.dufy.service;

import org.ssm.dufy.entity.User;

public interface UserService {
	
	void insert(User record);

	void update_flag(String id);

	User getUser(int id);
}
