package org.ssm.dufy.service;

import org.ssm.dufy.entity.Voter;
import org.ssm.dufy.entity.User;

public interface IUserService {

    int deleteByPrimaryKey(Integer id);

    void insert(User record);

    void update_flag(String id);

    User selectByName(String name);

    int updateByPrimaryKeySelective(User record);

    void updateById(User record);
    
    User getUser(int id);
}

