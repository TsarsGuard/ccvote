package org.ssm.dufy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssm.dufy.dao.CandidaDao;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.dao.VoteDao;
import org.ssm.dufy.entity.Candida;
import org.ssm.dufy.entity.Voter;
import org.ssm.dufy.entity.User;
import org.ssm.dufy.tool.UserUtil;

@Service
public class CandidaServiceImpl implements CandidaDao {

	@Autowired
	@Qualifier("c")
	private CandidaDao cDao;

	public void insert(Candida can) {
		cDao.insert(can);
	}

	public List<Candida> show() {
		return cDao.show();
	}



}
