package org.ssm.dufy.dao;

import org.springframework.stereotype.Repository;
import org.ssm.dufy.entity.User;

@Repository("u")
public interface IUserDao {
    int deleteByPrimaryKey(Integer id);

    void insert(User record);

    void update_flag(String id);

    User selectByName(String name);

    int updateByPrimaryKeySelective(User record);

    void updateById(User record);
    
    User getUser(int id);
}