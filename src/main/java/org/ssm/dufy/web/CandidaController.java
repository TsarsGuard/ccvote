package org.ssm.dufy.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.entity.Candida;
import org.ssm.dufy.entity.Voter;
import org.ssm.dufy.service.impl.CandidaServiceImpl;
import org.ssm.dufy.service.impl.VoteServiceImpl;
import org.ssm.dufy.tool.UserUtil;

@Controller
public class CandidaController {

	@Autowired
	private CandidaServiceImpl canService;

	@RequestMapping(value = "/addCandida", method = RequestMethod.POST)
	public String vote(@RequestParam(required = false) String candida,
			HttpServletRequest request, Model model) {
		Candida can = new Candida();
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		/******/
		String time = sdf.format(d);

		can.setName(candida);
		can.setTime(time);
		canService.insert(can);
		return "/WEB-INF/jsp/ " + "votesuccess.jsp";
	}

	@RequestMapping(value = "/showCandida", method = RequestMethod.POST)
	public String showCandida(@RequestParam(required = false) String candida,
			HttpServletRequest request, Model model) {
		ArrayList<Candida> list = (ArrayList<Candida>) canService.show();
		model.addAttribute("c_list", list);
		return "candida.jsp";
	}

}
