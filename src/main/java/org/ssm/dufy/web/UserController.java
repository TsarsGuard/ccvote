package org.ssm.dufy.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.entity.User;
import org.ssm.dufy.service.impl.IUserServiceImpl;
//import org.ssm.dufy.service.impl.IUserServiceImpl;
//import org.ssm.dufy.service.IUserService;
//import org.ssm.dufy.service.impl.IUserServiceImpl;
import org.ssm.dufy.tool.UserUtil;

@Controller
public class UserController {

	@Autowired
	public IUserDao UserDao;

	@Autowired
	private IUserServiceImpl userService;

	/**
	 * 登录
	 * 
	 * @param username
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/test1", method = RequestMethod.POST)
	public String test(@RequestParam(required = false) String username,
			HttpServletRequest request, Model model) {
		System.out.println("start");
		User u = new User();
		u = UserDao.selectByName(username);
		if (u == null)
			return "error";
		if (!u.getPwd().equals(request.getParameter("password")))
			return "error";
		model.addAttribute("name", username);
		UserUtil.setLoguser(u);
		System.out.println("end");
		return "/WEB-INF/jsp/"+"showName.jsp";
	}

	/**
	 * 添加新用户
	 * 
	 * @param username
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@RequestParam(required = false) String username,
			HttpServletRequest request, Model model) {
		// 判断是否为管理员
		if (UserUtil.getLoguser().getId() == "1") {
			User u = new User();
			String one = request.getParameter("password");
			String two = request.getParameter("password2");
			if (one.equals(two)) {
				u.setName(username);
				u.setPwd(one);
				userService.insert(u);
				return "showName";
			} else
				return "error";
		} else
			return "/WEB-INF/jsp/"+"notAdmin.jsp";
	}
	
	

	@RequestMapping(value = "/showname", method = RequestMethod.GET)
	public String showUserName(HttpServletRequest request, Model model) {
		return "/WEB-INF/jsp/"+"showName.jsp";
	}
}
