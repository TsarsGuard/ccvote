package org.ssm.dufy.service.impl;



import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.dao.VoteDao;
import org.ssm.dufy.entity.Voter;
import org.ssm.dufy.entity.User;
import org.ssm.dufy.service.IUserService;
import org.ssm.dufy.service.UserService;

@Service
public class IUserServiceImpl  implements UserService{

	@Autowired
	@Qualifier("u")
	private IUserDao udao;
	
	@Autowired
	@Qualifier("v")
	private VoteDao vdao;

	public void insert(User record) {
		udao.insert(record);
	}

	public void update_flag(String id) {
		udao.update_flag(id);	
	}

	public User getUser(int id) {
		// TODO Auto-generated method stub
		return null;
	}



}
