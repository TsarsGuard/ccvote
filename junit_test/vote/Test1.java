package vote;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.entity.User;
//import org.ssm.dufy.service.IUserService;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器 spring-test,junit
 */
@RunWith(SpringJUnit4ClassRunner.class)
// 告诉junit spring配置文件
@ContextConfiguration({ "classpath:applicationContext.xml"})
public class Test1 {
//	@Autowired
//	public IUserService userService;

	@Autowired
	public IUserDao UserDao;

	@Test
	public void test() {
		System.out.println("start");
		User u=new User();
		u=UserDao.getUser(2);
		System.out.println("end");
	}

}
