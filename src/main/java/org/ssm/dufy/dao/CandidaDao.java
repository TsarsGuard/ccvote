package org.ssm.dufy.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.ssm.dufy.entity.Candida;
import org.ssm.dufy.entity.Voter;

@Repository("c")
public interface CandidaDao {
	void insert(Candida can);
	List<Candida> show();
}
