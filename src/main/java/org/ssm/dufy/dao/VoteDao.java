package org.ssm.dufy.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.ssm.dufy.entity.VoteResult;
import org.ssm.dufy.entity.Voter;

@Repository("v")
public interface VoteDao {
	void insert(Voter can);

	List<VoteResult> showResult();
}
