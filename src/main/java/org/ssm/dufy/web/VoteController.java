package org.ssm.dufy.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.ssm.dufy.dao.IUserDao;
import org.ssm.dufy.entity.VoteResult;
import org.ssm.dufy.entity.Voter;
import org.ssm.dufy.service.impl.VoteServiceImpl;
import org.ssm.dufy.tool.UserUtil;

@Controller
public class VoteController {
	@Autowired
	private IUserDao UserDao;

	@Autowired
	private VoteServiceImpl voteService;

	@RequestMapping(value = "/votecandida", method = RequestMethod.POST)
	public String vote(@RequestParam(required = false) String votename,
			HttpServletRequest request, Model model) {
		if (UserUtil.getLoguser().getFlag() == 1)// 已完成投票
			return "votefail";
		else {
			Voter can = new Voter();
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			/******/
			String one = votename;
			String two = UserUtil.getLoguser().getName();
			String three = sdf.format(d);
			can.setCandida(one);
			can.setVoter(two);
			can.setTime(three);
			voteService.insert(can);
			return "/WEB-INF/jsp/" + "votesuccess.jsp";
		}
	}

	@RequestMapping(value = "/showResult", method = RequestMethod.POST)
	public String voteResult(@RequestParam(required = false) String votename,
			HttpServletRequest request, Model model) {

		ArrayList<VoteResult> v_list = (ArrayList<VoteResult>) voteService
				.showResult();
		model.addAttribute("v_list", v_list);
		return "result.jsp";
	}

}
